echo "initial run, installing autoproj, building CDFF deps"

export AUTOPROJ_BOOTSTRAP_IGNORE_NONEMPTY_DIR=1
export GIT_SSL_NO_VERIFY=true
ssh-keyscan gitlab.spaceapplications.com >> ~/.ssh/known_hosts

# dependencies
ruby ../autoproj_bootstrap git git@gitlab.spaceapplications.com:InFuse/cdff-buildconf
#cp ../config.yml .autoproj/

source env.sh
autoproj update
autoproj osdeps

#touch /build_deps
echo "exiting, please run again to install cdff and envire"
sleep 2
